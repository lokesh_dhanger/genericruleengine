package com.example.genericruleengine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenericRuleEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenericRuleEngineApplication.class, args);
	}

}
