package com.example.genericruleengine.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface DecisionTableService {

    public ResponseEntity<?> uploadDecisionTable(String projectName, String featureName, MultipartFile file);

    public ResponseEntity<?> validateDecisionTables(String projectName, String featureName);

}
