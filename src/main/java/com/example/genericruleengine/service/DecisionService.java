package com.example.genericruleengine.service;

public interface DecisionService {

    void evaluateDecision(String service, String feature, Object payload);
}
