package com.example.genericruleengine.service;

import org.springframework.http.ResponseEntity;

public interface CacheService {

    public ResponseEntity<?> clearCache(String projectName, String featureName);

}
