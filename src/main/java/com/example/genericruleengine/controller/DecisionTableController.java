package com.example.genericruleengine.controller;

import com.example.genericruleengine.service.DecisionTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.ws.Response;

@RestController
@RequestMapping("api")
public class DecisionTableController {

    @Autowired
    private DecisionTableService decisionTableService;

    /**
     *
     * @param projectName
     * @param featureName
     * @param file
     * @return
     */
    @PostMapping("upload/{projectName}/features/{featureName}")
    public ResponseEntity<?> uploadDecisionTable(@PathVariable String projectName, @PathVariable String featureName, MultipartFile file) {
        return decisionTableService.uploadDecisionTable(projectName, featureName, file);
    }

    /**
     *
     * @param projectName
     * @param featureName
     * @return
     */
    @GetMapping("validate/{projectName}/features/{featureName}")
    public ResponseEntity<?> validateDecisionTables(@PathVariable String projectName, @PathVariable String featureName) {
        return decisionTableService.validateDecisionTables(projectName, featureName);
    }

}
