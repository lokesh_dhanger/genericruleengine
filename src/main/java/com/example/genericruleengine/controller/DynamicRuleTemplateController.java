package com.example.genericruleengine.controller;

import com.example.genericruleengine.entity.*;
import com.example.genericruleengine.model.Condition;
import com.example.genericruleengine.model.Rule;
import com.example.genericruleengine.repository.*;
import com.example.genericruleengine.util.DroolsUtility;
import com.example.genericruleengine.util.PojoGeneratorUtil;
import javassist.CannotCompileException;
import lombok.extern.slf4j.Slf4j;
import org.drools.compiler.lang.api.DescrFactory;
import org.drools.compiler.lang.api.PackageDescrBuilder;
import org.drools.mvel.DrlDumper;
import org.kie.api.runtime.StatelessKieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("api")
public class DynamicRuleTemplateController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private AttributesRepository attributesRepository;

    @Autowired
    private ConditonsRepository conditonsRepository;

    @Autowired
    private RulesRepository rulesRepository;

    @Autowired
    private RuleTemplateRepository ruleTemplateRepository;


    //create new rule template
    @GetMapping("dynamicrules/{projectName}/features/{featureName}")
    public void dynamicRuleTemplate(@PathVariable String projectName, @PathVariable String featureName) {
        log.info("dynamicRuleTemplate() called projectName = {} featureName = {}", projectName, featureName);
        Optional<Project> project = projectRepository.findByNameAndFeature(projectName, featureName);

        if(!project.isPresent()) {
            log.error("no project found");
            return;
        }

        List<AttributesEntity> attributesEntityList = attributesRepository.findByProjectId(project.get());
        if(CollectionUtils.isEmpty(attributesEntityList)){
            log.error("no attribute in class, at least one attribute/property should be there");
            return;
        }

        //Class creation : start
        String className = projectName+"_"+featureName;
        String packageName = "com.example.genericruleengine.model";
        Map<String, Class<?>> props = new HashMap<>();
        attributesEntityList.forEach(attribute -> {
            props.put(attribute.getName(), PojoGeneratorUtil.getClassByType(attribute.getAttributeType()));
        });
        //add result attribute as well
        props.put("result", PojoGeneratorUtil.getClassByType(project.get().getGlobalType().toString()));
        log.info("props = {}", props);
        try {
            Class<?> generatedClass = PojoGeneratorUtil.generate(packageName+"."+PojoGeneratorUtil.capitaliseString(className), props);
            Object object = generatedClass.newInstance();

            //get rules of project
            List<RulesEntity> rulesEntityList = rulesRepository.findByProjectId(project.get());
            if (CollectionUtils.isEmpty(rulesEntityList)){
                log.error("no rules present");
                return;
            }

            List<ConditionsEntity> conditionsEntityList = conditonsRepository.findByProjectId(project.get());
            if(CollectionUtils.isEmpty(conditionsEntityList)){
                log.error("no conditions present");
                return;
            }
            log.info("conditionsEntityList {}", conditionsEntityList);
            List<Rule> rules = new ArrayList<Rule>();
            for (RulesEntity rulesEntity : rulesEntityList) {
                Rule rule = new Rule(rulesEntity.getRuleName(), generatedClass.getName(), generatedClass.getSimpleName());
                rule.setDataObject(generatedClass.getSimpleName());
                List<ConditionsEntity> ruleConditions = conditionsEntityList.stream().filter(condition->condition.getRuleId().getId().equals(rulesEntity.getId())).collect(Collectors.toList());
                if(!CollectionUtils.isEmpty(ruleConditions)){
                    //prepare conditions
                    List<Condition> conditions = new ArrayList<>();
                    for (ConditionsEntity conditionsEntity : ruleConditions) {
                        Condition condition = new Condition();
                        condition.setProperty(conditionsEntity.getProperty());
                        condition.setOperator(conditionsEntity.getOperator());
                        List<AttributesEntity> collect = attributesEntityList.stream().filter(attribute -> attribute.getName().equalsIgnoreCase(condition.getProperty())).collect(Collectors.toList());
                        condition.setValue(PojoGeneratorUtil.getWrapperValue(conditionsEntity.getValue(), collect.get(0)));  // set proper value here with proper data type
                        conditions.add(condition);
                    }
                    rule.setConditions(conditions);
                    rule.setAction(rulesEntity.getResult());
                    rule.setSimpleName(generatedClass.getSimpleName());
                    rule.setCanonicalName(generatedClass.getCanonicalName());
                    log.info("rule = {}", rule);

                    log.info("**************************************************");
                    PackageDescrBuilder packageDescrBuilder = DescrFactory.newPackage();


                    packageDescrBuilder
                            .name(packageName)
                            .newRule()
                            .name(rulesEntity.getRuleName())
                            .lhs()
                            .pattern(generatedClass.getSimpleName())
                            .constraint("age < 18")

                            .id("$a", true)
                            .end()
//                            .pattern().id("$a", false).end()
                            .end()
                            .rhs("$a.result( false );")
                            //.rhs("insert(new Person())")
                            .end();


                    String rules_ = new DrlDumper().dump(packageDescrBuilder.getDescr());
                    log.info("testing rules_ = \n{}", rules_);

                    log.info("**************************************************");

                    rules.add(rule);
                } else {
                    //no conditions present for this rule
                    //do not add this rule for evaluation
                    log.warn("no conditions present for this rule, this rule won't be evaluated ");
                    log.warn("rule name = {}", rule.getName());
                }
            }



            //create drl file
            Optional<RuleTemplateEntity> ruleTemplate = ruleTemplateRepository.findByProjectId(project.get());
            if (!ruleTemplate.isPresent()){
                log.error("no rule template found for this project");
                return;
            }
            File drlFile = new File("rules/"+generatedClass.getSimpleName()+".drl");
            drlFile.createNewFile(); // if file already exists will do nothing

            //prepare template here
            BufferedWriter writer = new BufferedWriter(new FileWriter("rules/"+generatedClass.getSimpleName()+".drl"));
            writer.write(ruleTemplate.get().getTemplate());
            writer.close();

//            List<String> attributes = attributesEntityList.stream().map(AttributesEntity::getName).collect(Collectors.toList());
//            Map<String, Object> drlProperties = new HashMap<>();
//            drlProperties.put("listOfAttributes", attributes);

            //prepare drl string
            DroolsUtility utility = new DroolsUtility();
            try {
                StatelessKieSession session = utility.loadSession(rules, "rules/"+generatedClass.getSimpleName()+".drl");
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (CannotCompileException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    //validate rule template

}
