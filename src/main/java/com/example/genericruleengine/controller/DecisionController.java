package com.example.genericruleengine.controller;

import com.example.genericruleengine.service.DecisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("decisions")
public class DecisionController {

    @Autowired
    private DecisionService decisionService;

    @GetMapping("services/{service}/features/{feature}")
    public void evaluateDecision(@PathVariable String service, @PathVariable String feature, @RequestBody Object payload) {
        decisionService.evaluateDecision(service, feature, payload);
    }

}
