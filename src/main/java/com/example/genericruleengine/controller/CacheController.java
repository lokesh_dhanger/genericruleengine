package com.example.genericruleengine.controller;

import com.example.genericruleengine.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class CacheController {

    @Autowired
    private CacheService cacheService;

    @GetMapping("clearcache")
    public ResponseEntity<?> rulesUpdated(String projectName, String featureName) {
        return cacheService.clearCache(projectName, featureName);
    }

}
