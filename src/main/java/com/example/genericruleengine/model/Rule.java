package com.example.genericruleengine.model;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class Rule {

    private final String name;
    private String canonicalName;
    private String simpleName;
    private String object;
    private List<Condition> conditions = new ArrayList<>();
    private String action;

    public Rule(String name, String canonicalName, String simpleName) {
        this.name = name;
        this.canonicalName = canonicalName;
        this.simpleName = simpleName;
    }

    public enum Attribute {
        /**
         * Name of the rule.
         */
        RULE_NAME("name"),
        /**
         * canonical name of the class.
         */
        CANONICAL_NAME("canonicalName"),
        /**
         * simpleName of the class.
         */
        SIMPLE_NAME("simpleName"),

        /**
         * Object with data to be processed.
         */
        DATA_OBJECT("object"),
        /**
         * Conditional expression.
         */
        CONDITIONAL("conditional"),
        /**
         * Action to take.
         */
        ACTION("action");

        /**
         * Name used in template to assign each attirbute.
         */
        private final String name;

        private Attribute(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }


    @Override
    public String toString() {
        StringBuilder me = new StringBuilder("[" + this.getClass().getName());
        me.append(" | name = ");
        me.append(name);
        me.append(" | canonicalName = ");
        me.append(canonicalName);
        me.append(" | simpleName = ");
        me.append(simpleName);
        me.append(" | object = ");
        me.append(object);
        me.append(" | conditions = ");
        me.append(((conditions == null) ? "null" : conditions.size()));
        me.append(" | action = ");
        me.append(action);
        me.append("]");

        return me.toString();
    }


    public String conditionAsDRL() throws IllegalStateException, IllegalArgumentException {
        if ((conditions == null) || (conditions.isEmpty())) {
            throw new IllegalStateException("You must declare at least one condition to be evaluated.");
        }
        StringBuilder drl = new StringBuilder();
        //For each condition of this rule, we create its textual representation
        for (int i = 0; i < conditions.size(); i++) {
            Condition condition = conditions.get(i);
            drl.append("(");
            drl.append(condition.buildExpression());
            drl.append(")");
            if ((i + 1) < conditions.size()) {
                drl.append(" && ");
            }
        }
        return drl.toString();
    }

    public Map<String, Object> asMap() throws IllegalStateException {
        log.info("asMap() :: name {}", name);
        log.info("asMap() :: canonicalName {}", canonicalName);
        log.info("asMap() :: simpleName {}", simpleName);
        log.info("asMap() :: object {}", object);
        log.info("asMap() :: action {}", action);
        log.info("asMap() :: conditional {}", conditions);

        if ((name == null) || (object == null) || (action == null)) {
            throw new IllegalArgumentException("The rule has no name, object to be evaluated or action to be accomplished.");
        }
        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put(Rule.Attribute.RULE_NAME.toString(), name);
        attributes.put(Rule.Attribute.CANONICAL_NAME.toString(), canonicalName);
        attributes.put(Rule.Attribute.SIMPLE_NAME.toString(), simpleName);
        attributes.put(Rule.Attribute.DATA_OBJECT.toString(), object);
        attributes.put(Rule.Attribute.CONDITIONAL.toString(), conditionAsDRL());
        attributes.put(Rule.Attribute.ACTION.toString(), action);
        return attributes;
    }

    /**
     * Create new condition and add it to this rule.
     *
     * @param property Object property to be evaluated.
     * @param operator Operator used to compare the data.
     * @param value Value to be evaluated.
     * @return Condition created.
     */
    public Condition addCondition(String property, Condition.Operator operator, Object value) {
        Condition condition = new Condition(property, operator, value);
        conditions.add(condition);
        return condition;
    }

    public String getName() {
        return name;
    }

    public String getDataObject() {
        return object;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public Condition getCondition() {
        if ((conditions == null) || (conditions.isEmpty())) {
            return null;
        } else {
            return conditions.get(0);
        }
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public void setCondition(Condition condition) {
        conditions = new ArrayList<>();
        conditions.add(condition);
    }

    public String getAction() {
        return action;
    }

    public void setDataObject(String dataObject) {
        this.object = dataObject;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCanonicalName() {
        return canonicalName;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setCanonicalName(String canonicalName) {
        this.canonicalName = canonicalName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }
}
