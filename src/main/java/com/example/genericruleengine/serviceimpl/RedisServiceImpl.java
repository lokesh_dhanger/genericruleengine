package com.example.genericruleengine.serviceimpl;


import com.example.genericruleengine.config.RedisConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class RedisServiceImpl {


    @Autowired
    private RedisConfigService redisConfigService;

    public void insert(Object key, Object hash, Object value) {
        RedisTemplate template = redisConfigService.redisTemplate();
        template.opsForHash().put(key, hash, value);
        log.info("Insert [" + value + "] success!");
        log.info("Hash size is : " + template.opsForHash().size(key));
    }

    public void batchInsert(Object key, Map values) {
        RedisTemplate template = redisConfigService.redisTemplate();
        template.opsForHash().putAll(key, values);
        log.info("BatchInsert [" + values + "] success!");
        log.info("Hash size is : " + template.opsForHash().size(key));
    }

    public void insertIfAbsent(Object key, Object hash, Object value) {
        RedisTemplate template = redisConfigService.redisTemplate();
        template.opsForHash().putIfAbsent(key, hash, value);
        log.info("InsertIfAbsent [" + value + "] success!");
        log.info("Hash size is : " + template.opsForHash().size(key));
    }

    public Map<Object, Object> findAll(Object key) {
        RedisTemplate template = redisConfigService.redisTemplate();
        Map<Object, Object> values = template.opsForHash().entries(key);
        log.info("All Values [" + values + "]");
        log.info("FindAll User size is : " + values.size());
        return values;
    }

    public Object findOne(Object key, Object hash) {
        RedisTemplate template = redisConfigService.redisTemplate();
        Object value = template.opsForHash().get(key, hash);
        log.info("Find one : " + value);
        return value;
    }

    public Set<Object> findAllKeys(Object key) {
        RedisTemplate template = redisConfigService.redisTemplate();
        Set<Object> values = template.opsForHash().keys(key);
        log.info("Find : " + values.size() + " values :" + values);
        return values;
    }

    public void scan(Object key, ScanOptions scanOptions) {
        RedisTemplate template = redisConfigService.redisTemplate();
        template.opsForHash().scan(key, scanOptions); //ScanOptions.None
    }

    public void incrementDouble(Object key) {
        String hashKey = UUID.randomUUID().toString();
        RedisTemplate template = redisConfigService.redisTemplate();
        Double value = template.opsForHash().increment(key, hashKey, Double.valueOf("30"));
        log.info(key + ":" + hashKey + " has value :" + value);

        Double delta = Double.valueOf("30.3");
        Double value1 = template.opsForHash().increment(key, hashKey, delta);
        log.info(key + ":" + hashKey + " has value: " + value1 + ", after increment " + delta);
    }

    public void incrementLong(Object key) {
        String hashKey = UUID.randomUUID().toString();
        RedisTemplate template = redisConfigService.redisTemplate();
        long value1 = template.opsForHash().increment(key, hashKey, 30l);
        log.info(key + ":" + hashKey + " has value :" + value1);
        long delta = 20l;
        long value2 = template.opsForHash().increment(key, hashKey, delta);
        log.info(key + ":" + hashKey + " has value: " + value2 + ", after increment " + delta);
    }

    public Object getValue(final String key) {
        RedisTemplate template = redisConfigService.redisTemplate();
        return template.opsForValue().get(key);
    }



    public List<String> getAllKeys(String pattern) {
        RedisTemplate template = redisConfigService.redisTemplate();
        Set<String> redisKeys = template.keys(pattern + "*");
        // Store the keys in a List
        List<String> keysList = new ArrayList<>();
        Iterator<String> it = redisKeys.iterator();
        while (it.hasNext()) {
            String data = it.next();
            keysList.add(data);
        }
        return keysList;
    }

    public void setValueWithoutExpiry(final String key, final Object value) {
        try {
            RedisTemplate template = redisConfigService.redisTemplate();
            template.opsForValue().set(key, value);
        }catch (Exception ex){
//            log.log(Level.SEVERE,"Exception while storing value in Redis:"+ex.getMessage(),ex);
            log.error("Exception while getting redisObject :  {}", ex.getMessage());
            log.error("exceptioon = {}", ex);
        }
    }

    public void setValue(final String key, final Object value, final Integer timeInSeconds) {
        try {
            RedisTemplate template = redisConfigService.redisTemplate();
            template.opsForValue().set(key, value);
            // set a expire for a message
            template.expire(key, timeInSeconds, TimeUnit.SECONDS);
        }catch (Exception ex){
//            log.log(Level.SEVERE,"Exception while storing value in Redis:"+ex.getMessage(),ex);
            log.error("Exception while getting redisObject :  {}", ex.getMessage());
            log.error("exceptioon = {}", ex);
        }
    }


    /**
     * This is for getting value and setting null after getting value...
     * @param key
     * @return Object
     */
    public Object getAndSet(final String key)
    {
        Object redisVaue = null;
        try {
            RedisTemplate template = redisConfigService.redisTemplate();
            redisVaue = template.opsForValue().getAndSet(key, null);
        }catch (Exception ex){
//            log.log(Level.SEVERE, "Exception while getting redisObject : "+ex.getMessage(),ex);

            log.error("Exception while getting redisObject :  {}", ex.getMessage());
            log.error("exceptioon = {}", ex);
        }
        return redisVaue;
    }


    public void remove(final String key) {
        RedisTemplate template = redisConfigService.redisTemplate();
        template.delete(key);
    }

    public Boolean changeKeyExpiry(final String key, final Date date) {
        RedisTemplate template = redisConfigService.redisTemplate();
        return template.expireAt(key,date);
    }

    public void flush() {
        RedisTemplate template = redisConfigService.redisTemplate();
        template.getConnectionFactory().getConnection().flushAll();
    }
}