package com.example.genericruleengine.serviceimpl;

import com.example.genericruleengine.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CacheServiceImpl implements CacheService {

    @Autowired
    private RedisServiceImpl redisService;

    @Override
    public ResponseEntity<?> clearCache(String projectName, String featureName) {
        log.info("clearCache() called with projectName = {} featureName = {}", projectName, featureName);
        Object value = redisService.getValue(projectName + "_" + featureName);
        if(value!=null){
            redisService.remove(projectName+"_"+featureName);
            log.info("key found in cache, key is cleared");
            return ResponseEntity.ok("cache clear");
        }
        log.warn("key not found in cache");
        return ResponseEntity.notFound().build();
    }

}
