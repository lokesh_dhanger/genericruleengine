package com.example.genericruleengine.serviceimpl;

import com.example.genericruleengine.config.DecisionTableDroolsConfig;
import com.example.genericruleengine.drools.Globals;
import com.example.genericruleengine.drools.InputFacts;
import com.example.genericruleengine.entity.Project;
import com.example.genericruleengine.repository.ProjectRepository;
import com.example.genericruleengine.service.DecisionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class DecisionServiceImpl implements DecisionService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DecisionTableDroolsConfig decisionTableDroolsConfig;

    @Override
    public void evaluateDecision(String service, String feature, Object payload) {
        log.info("evaluateDecision() called with service = {}, feature = {} payload = {}", service, feature, payload);

        Optional<Project> project = projectRepository.findByNameAndFeature(service, feature);
        if (!project.isPresent()){
            log.warn("project info not present " );
            return;
        }

        Map<String, Object> map = objectMapper.convertValue(payload, Map.class);
        log.info("object to map conversion = {}", map);
        InputFacts inputFacts = new InputFacts();
        inputFacts.setInputFactsMap(map);

        log.info("inputFacts {}" , inputFacts);

        Object result = Globals.getObjectType(project.get().getGlobalType().toString());
        log.info("objectType {}", result);

//        Boolean data = true;

        decisionTableDroolsConfig.loadKieContainer(service, feature);
        KieContainer kieContainer = decisionTableDroolsConfig.getKieContainer(service + "_" + feature);
        StatelessKieSession statelessKieSession = kieContainer.newStatelessKieSession();
        statelessKieSession.setGlobal("result", result);
        statelessKieSession.execute(inputFacts);
        log.info("object type = {} ", result);

    }
}
