package com.example.genericruleengine.serviceimpl;

import com.example.genericruleengine.service.DecisionTableService;
import lombok.extern.slf4j.Slf4j;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.io.Resource;
import org.kie.internal.io.ResourceFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

@Slf4j
@Service
public class DecisionTableServiceImpl implements DecisionTableService {

    private final Path root = Paths.get("rules/decisiontables");

    @Override
    public ResponseEntity<?> uploadDecisionTable(String projectName, String featureName, MultipartFile file) {
        log.info("uploadDecisionTable() called projectName = {} featureName = {} file = {}", projectName, featureName, file);
        if (file==null){
            log.warn("decision table file is missing");
            return ResponseEntity.badRequest().body("Decision table missing in request");
        }
        log.info("name {}", file.getName());
        log.info("content type  {}", file.getContentType());
        log.info("original final name = {}", file.getOriginalFilename());
        log.info("size {}", file.getSize());

        String originalFilename = file.getOriginalFilename();
        String[] split = originalFilename.split("\\.");
        log.info("split array {}", Arrays.asList(split) );
        String extension = split[split.length - 1];
        log.info("extension {}", extension);
        log.info("InputType.XLS.toString() {}", InputType.XLS.toString());

        if(!"XLS".equalsIgnoreCase(extension)){
            log.warn("decision table file extension is incorrect , filename = {}", file.getOriginalFilename());
            return ResponseEntity.badRequest().body("only XLS files supported");
        }
        try {
            Files.copy(file.getInputStream(), this.root.resolve(file.getOriginalFilename()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("file uploaded successfully.");
    }

    @Override
    public ResponseEntity<?> validateDecisionTables(String projectName, String featureName) {
        log.info("validateDecisionTables() :: projectName = {} featureName = {}", projectName, featureName);
        //download decision table file from cloud storage service (aws etc)
        File excelFile = new File("C:\\Users\\lokesh.dhanger\\IdeaProjects\\gre-decision-tables\\rules\\decisiontables\\"+projectName+ "_"+featureName+".XLS");
        Resource resource = ResourceFactory.newFileResource(excelFile);
        SpreadsheetCompiler spreadsheetCompiler = new SpreadsheetCompiler();
        String drlString = "";
        try {
            drlString = spreadsheetCompiler.compile(resource.getInputStream(), InputType.XLS);
            log.info("drlString = \n{} ", drlString);
            return ResponseEntity.ok("decision table file is validated successfully");
        } catch (IOException e) {
            log.error("exception while spreadsheet compilation, message = {}", e.getMessage());
            e.printStackTrace();
        }
        return ResponseEntity.ok("decision table file is malformed, please check any syntax error in decision table");
    }
}
