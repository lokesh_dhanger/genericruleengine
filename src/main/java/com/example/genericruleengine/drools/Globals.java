package com.example.genericruleengine.drools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public enum Globals {

    BOOLEAN("BOOLEAN"),
    STRING("STRING"),
    LIST("LIST"),
    MAP("MAP"),
    SET("SET"),
    INTEGER("INTEGER"),
    FLOAT("FLOAT");

    private final String value;
    private static Map<String, Globals> constants = new HashMap<String, Globals>();

    static {
        for (Globals c : values()) {
            constants.put(c.value, c);
        }
    }

    private Globals(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public static Globals fromValue(String value) {
        Globals constant = constants.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        } else {
            return constant;
        }
    }

    public static Object getObjectType(String type) {
        Globals globals = constants.get(type);
        Object globalType =  null;
        switch (globals){
            case LIST:globalType = new ArrayList<>(); break;
            case BOOLEAN:globalType = false; break;
            case MAP:globalType = new HashMap<>(); break;
            case SET:globalType = new HashSet<>(); break;
            case STRING:globalType = new String(); break;
            default:
                globalType=new String();
                break;
        }
        return globalType;
    }

}
