package com.example.genericruleengine.drools;

import java.util.HashMap;
import java.util.Map;

public enum Operator {

    NOT_EQUAL_TO("NOT_EQUAL_TO"),
    EQUAL_TO("EQUAL_TO"),
    MORE_THAN("MORE_THAN"),
    LESS_THAN("LESS_THAN"),
    MORE_THAN_EQUAL_TO("MORE_THAN_EQUAL_TO"),
    LESS_THAN_EQUAL_TO("LESS_THAN_EQUAL_TO");

    private final String value;
    private static Map<String, Operator> constants = new HashMap<String, Operator>();

    static {
        for (Operator c : values()) {
            constants.put(c.value, c);
        }
    }

    private Operator(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public static Operator fromValue(String value) {
        Operator constant = constants.get(value);
        if (constant == null) {
            throw new IllegalArgumentException(value);
        } else {
            return constant;
        }
    }

}
