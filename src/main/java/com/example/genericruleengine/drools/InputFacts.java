package com.example.genericruleengine.drools;


import java.util.Map;

/**
 *
 * this class is used as global variable in drools decision tables
 *
 */
public class InputFacts {

    private Map<String, Object> inputFactsMap;

    public Map<String, Object> getInputFactsMap() {
        return inputFactsMap;
    }

    public void setInputFactsMap(Map<String, Object> inputFactsMap) {
        this.inputFactsMap = inputFactsMap;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InputFacts{");
        sb.append("inputFactsMap=").append(inputFactsMap);
        sb.append('}');
        return sb.toString();
    }
}
