package com.example.genericruleengine.config;

import com.example.genericruleengine.serviceimpl.RedisServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class DecisionTableDroolsConfig {

    @Autowired
    private RedisServiceImpl redisService;

    private Map<String, KieContainer> kieContainerMap = new HashMap<>();

    public Map<String, KieContainer> getKieContainerMap() {
        return kieContainerMap;
    }

    public void setKieContainerMap(Map<String, KieContainer> kieContainerMap) {
        this.kieContainerMap = kieContainerMap;
    }


    public static KieModule getKieModule(KieServices kieServices, File excelFile) {
        Resource resource = ResourceFactory.newFileResource(excelFile);
        SpreadsheetCompiler spreadsheetCompiler = new SpreadsheetCompiler();
        String drlString = "";
        try {
            drlString = spreadsheetCompiler.compile(resource.getInputStream(), InputType.XLS);
            log.info("drlString = {} ", drlString);
        } catch (IOException e) {
            log.error("exception while spreadsheet compilation, message = {}", e.getMessage());
            e.printStackTrace();
        }
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieFileSystem.write(resource);
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        kieBuilder.buildAll();
        return kieBuilder.getKieModule();
    }

    public void loadKieContainer(String serviceName, String feature) {
        String fileName = serviceName+ "_"+feature+".XLS";
        //check in cache, if there is any key for this fileName.
        //if yes, then no need to load any kieContainer
        //if no key present, then download new excel file and compile it
        Object object = redisService.getValue(serviceName + "_" + feature);
        if(object!=null) {
            log.info("object = {} ", object);

        } else {
            //download decision table file from cloud storage service (aws etc)
            File excelFile = new File("C:\\Users\\lokesh.dhanger\\IdeaProjects\\gre-decision-tables\\rules\\decisiontables\\"+serviceName+ "_"+feature+".XLS");
            KieServices kieServices = KieServices.Factory.get();
            KieModule kieModule = getKieModule(kieServices, excelFile);
            kieServices.getRepository().addKieModule(kieModule);

            if(this.kieContainerMap.containsKey(excelFile.getName())){
                this.kieContainerMap.remove(excelFile.getName());
            }
            log.info("excelFile.getName() = {}", excelFile.getName());
            this.kieContainerMap.put(serviceName + "_" + feature, kieServices.newKieContainer(kieModule.getReleaseId()));

            //add key in redis, without expiry
            redisService.setValueWithoutExpiry(serviceName + "_" + feature, true);
        }


    }

    public KieContainer getKieContainer(String serviceAndFeature) {
        log.info("serviceAndFeature = {}", serviceAndFeature);
        log.info("kieContainerMap = {}", this.kieContainerMap);
        return this.kieContainerMap.get(serviceAndFeature);
    }

}
