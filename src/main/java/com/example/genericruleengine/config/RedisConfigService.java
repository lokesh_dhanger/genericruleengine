package com.example.genericruleengine.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.JedisPoolConfig;

@Slf4j
@Service
@Configuration
@EnableCaching
public class RedisConfigService {

    @Value("${redis.ip}")
    private String redisIp;
    @Value("${redis.port}")
    private String redisPort;
    @Value("${redis.default.expiry}")
    private String redisDefaultExpiry;
    @Value("${redis.maxTotal}")
    private int redisMaxTotal;
    @Value("${redis.maxIdle}")
    private int maxIdle;
    @Value("${redis.password}")
    private String redisPassword;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        log.info("propertySourcesPlaceholderConfigurer() called");
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        log.info("jedisConnectionFactory() called");
        JedisConnectionFactory factory = new JedisConnectionFactory();
        JedisPoolConfig poolConfig=new JedisPoolConfig();
        poolConfig.setMaxTotal(redisMaxTotal);
        poolConfig.setMaxIdle(maxIdle);
        poolConfig.setBlockWhenExhausted(true);

        /*JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(50);
        jedisPool = new JedisPool(
                poolConfig,
                redisIp,
                Integer.parseInt(redisPort),
                20000,
                null
        );*/
        factory.setHostName(redisIp);
        factory.setPort(Integer.parseInt(redisPort));
//        factory.setPassword(redisPassword);
        factory.setUsePool(true);
        factory.setPoolConfig(poolConfig);

//        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
//        redisStandaloneConfiguration.setHostName(redisIp);
//        redisStandaloneConfiguration.setPort(Integer.parseInt(redisPort));
//        redisStandaloneConfiguration.setDatabase(0);
//        redisStandaloneConfiguration.setPassword(RedisPassword.of(redisPassword));

//        JedisClientConfiguration.JedisClientConfigurationBuilder jedisClientConfiguration = JedisClientConfiguration.builder();
//        jedisClientConfiguration.connectTimeout(Duration.ofSeconds(60));// 60s connection timeout
//
//        JedisConnectionFactory jedisConFactory = new JedisConnectionFactory(redisStandaloneConfiguration,
//                jedisClientConfiguration.build());
//
//        return jedisConFactory;

        return factory;
    }

    @Bean
    public RedisTemplate<Object, Object> redisTemplate() {
        log.info("redisTemplate() called");
        RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<Object, Object>();
        redisTemplate.setConnectionFactory(jedisConnectionFactory());
        return redisTemplate;
    }

    /*@Bean
    public CacheManager cacheManager() {
        return new RedisCacheManager(redisTemplate());
    }*/

}