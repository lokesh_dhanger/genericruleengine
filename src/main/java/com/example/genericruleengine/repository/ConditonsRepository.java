package com.example.genericruleengine.repository;

import com.example.genericruleengine.entity.ConditionsEntity;
import com.example.genericruleengine.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConditonsRepository extends JpaRepository<ConditionsEntity, Long> {

    List<ConditionsEntity> findByProjectId(Project project);

}
