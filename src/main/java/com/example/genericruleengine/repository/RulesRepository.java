package com.example.genericruleengine.repository;

import com.example.genericruleengine.entity.Project;
import com.example.genericruleengine.entity.RulesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RulesRepository extends JpaRepository<RulesEntity, Long> {

    List<RulesEntity> findByProjectId(Project project);
}
