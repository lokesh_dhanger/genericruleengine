package com.example.genericruleengine.repository;

import com.example.genericruleengine.entity.Project;
import com.example.genericruleengine.entity.RuleTemplateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RuleTemplateRepository extends JpaRepository<RuleTemplateEntity, Long> {


    Optional<RuleTemplateEntity> findByProjectId(Project project);
}
