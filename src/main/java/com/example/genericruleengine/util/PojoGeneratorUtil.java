package com.example.genericruleengine.util;

import com.example.genericruleengine.entity.AttributesEntity;
import com.sun.org.apache.xpath.internal.operations.Bool;
import javassist.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.sql.Date;
import java.util.Map;

@Slf4j
public class PojoGeneratorUtil {

    public static Class generate(String className, Map<String, Class<?>> props) throws CannotCompileException {

        ClassPool classPool = ClassPool.getDefault();
        CtClass ctClass = classPool.makeClass(className);
        //implement interface to class
        ctClass.addInterface(PojoGeneratorUtil.resolveCtClass(Serializable.class));

        //add properties
        for(Map.Entry<String, Class<?>> entry : props.entrySet()) {
            //add field
            ctClass.addField(new CtField(PojoGeneratorUtil.resolveCtClass(entry.getValue()), entry.getKey(), ctClass));
            //add getter
            ctClass.addMethod(generateGetter(ctClass, entry.getKey(), entry.getValue()));
            //add setter
            ctClass.addMethod(generateSetter(ctClass, entry.getKey(), entry.getValue()));

        }
        return ctClass.toClass();
    }

    static CtMethod generateSetter(CtClass declaringClass, String fieldName, Class<?> fieldClass) throws CannotCompileException {
        String setterName = "set"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("public void ")
                .append(setterName)
                .append("(")
                .append(fieldClass.getName())
                .append(" ")
                .append(fieldName)
                .append(")")
                .append("{")
                .append("this.")
                .append(fieldName)
                .append("=")
                .append(fieldName)
                .append(";")
                .append("}");
        return CtMethod.make(stringBuffer.toString(), declaringClass);
    }

    static CtMethod generateGetter(CtClass declaringClass, String fieldName, Class<?> fieldClass) throws CannotCompileException {
        String getterName = "get"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("public ")
                .append(fieldClass.getName())
                .append(" ")
                .append(getterName)
                .append("(){")
                .append("return this.")
                .append(fieldName)
                .append(";")
                .append("}");
        return CtMethod.make(stringBuffer.toString(), declaringClass);
    }


    static CtClass resolveCtClass(Class clazz) {
        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass = null;
        try {
            ctClass = pool.get(clazz.getName());
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return  ctClass;
    }

    public static Class getClassByType(String type) {
        if("String".equalsIgnoreCase(type))
            return String.class;
        if("Boolean".equalsIgnoreCase(type))
            return Boolean.class;
        if("Integer".equalsIgnoreCase(type))
            return Integer.class;
        if("Float".equalsIgnoreCase(type))
            return Float.class;
        if("Character".equalsIgnoreCase(type))
            return Character.class;
        return String.class;
    }

    public static Object getWrapperValue(String value, AttributesEntity attributesEntity) {
        if("String".equalsIgnoreCase(attributesEntity.getAttributeType()))
            return String.valueOf(value);
        if("Boolean".equalsIgnoreCase(attributesEntity.getAttributeType()))
            return Boolean.valueOf(value);
        if("Integer".equalsIgnoreCase(attributesEntity.getAttributeType()))
            return Integer.valueOf(value);
        if("Float".equalsIgnoreCase(attributesEntity.getAttributeType()))
            return Float.valueOf(value);
        if("Date".equalsIgnoreCase(attributesEntity.getAttributeType()))
            return Date.valueOf(value);
        return String.valueOf(value);
    }

    public static String capitaliseString(String str) {
        str = str.replaceAll("[_]", "");
        log.info("after replacing _ {}", str);
        str = str.isEmpty()?str:Character.toUpperCase(str.charAt(0))+str.substring(1).toLowerCase();
        log.info("final str = {}", str);
        return str;
    }

}
