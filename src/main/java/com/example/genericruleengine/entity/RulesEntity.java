package com.example.genericruleengine.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Table(name = "rules")
@Entity
public class RulesEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "rule_name")
    private String ruleName;

    @Column(name = "enabled")
    private Boolean enabled;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "project_id")
    private Project projectId;

    @Column(name = "result")
    private String result;

    @Column(name = "created_on")
    @CreationTimestamp
    private Date createdOn;

    @Column(name = "updated_on")
    @UpdateTimestamp
    private Date updatedOn;

    @PrePersist
    protected void onCreate() {
        this.createdOn = new Date();
        this.updatedOn = new Date();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Project getProjectId() {
        return projectId;
    }

    public void setProjectId(Project projectId) {
        this.projectId = projectId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RulesEntity that = (RulesEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(ruleName, that.ruleName) && Objects.equals(enabled, that.enabled) && Objects.equals(projectId, that.projectId) && Objects.equals(result, that.result) && Objects.equals(createdOn, that.createdOn) && Objects.equals(updatedOn, that.updatedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ruleName, enabled, projectId, result, createdOn, updatedOn);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RulesEntity{");
        sb.append("id=").append(id);
        sb.append(", ruleName='").append(ruleName).append('\'');
        sb.append(", enabled=").append(enabled);
        sb.append(", projectId=").append(projectId);
        sb.append(", result='").append(result).append('\'');
        sb.append(", createdOn=").append(createdOn);
        sb.append(", updatedOn=").append(updatedOn);
        sb.append('}');
        return sb.toString();
    }
}
