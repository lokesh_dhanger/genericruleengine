package com.example.genericruleengine.entity;

import com.example.genericruleengine.drools.Globals;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity(name = "projects")
@Table(name = "projects")
public class Project implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "feature", nullable = false)
    private String feature;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "global_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private Globals globalType;

    @Column(name = "description")
    private String description;

    @Column(name = "created_on")
    @CreationTimestamp
    private Date createdOn;

    @Column(name = "updated_on")
    @UpdateTimestamp
    private Date updatedOn;

    @PrePersist
    protected void onCreate() {
        this.createdOn = new Date();
        this.updatedOn = new Date();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Globals getGlobalType() {
        return globalType;
    }

    public void setGlobalType(Globals globalType) {
        this.globalType = globalType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return enabled == project.enabled && Objects.equals(id, project.id) && Objects.equals(name, project.name) && Objects.equals(feature, project.feature) && globalType == project.globalType && Objects.equals(description, project.description) && Objects.equals(createdOn, project.createdOn) && Objects.equals(updatedOn, project.updatedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, feature, enabled, globalType, description, createdOn, updatedOn);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Project{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", feature='").append(feature).append('\'');
        sb.append(", enabled=").append(enabled);
        sb.append(", globalType=").append(globalType);
        sb.append(", description='").append(description).append('\'');
        sb.append(", createdOn=").append(createdOn);
        sb.append(", updatedOn=").append(updatedOn);
        sb.append('}');
        return sb.toString();
    }
}
