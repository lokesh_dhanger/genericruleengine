package com.example.genericruleengine.entity;

import com.example.genericruleengine.drools.Operator;
import com.example.genericruleengine.model.Condition;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity(name = "conditions")
@Table(name = "conditions")
public class ConditionsEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "property")
    private String property;

    @Column(name = "value")
    private String value;

    @Column(name = "operator", nullable = false)
    @Enumerated(EnumType.STRING)
    private Condition.Operator operator;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "project_id")
    private Project projectId;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "rule_id")
    private RulesEntity ruleId;

    @Column(name = "description")
    private String description;

    @Column(name = "created_on")
    @CreationTimestamp
    private Date createdOn;

    @Column(name = "updated_on")
    @UpdateTimestamp
    private Date updatedOn;

    @PrePersist
    protected void onCreate() {
        this.createdOn = new Date();
        this.updatedOn = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Condition.Operator getOperator() {
        return operator;
    }

    public void setOperator(Condition.Operator operator) {
        this.operator = operator;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Project getProjectId() {
        return projectId;
    }

    public void setProjectId(Project projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public RulesEntity getRuleId() {
        return ruleId;
    }

    public void setRuleId(RulesEntity ruleId) {
        this.ruleId = ruleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConditionsEntity that = (ConditionsEntity) o;
        return enabled == that.enabled && Objects.equals(id, that.id) && Objects.equals(property, that.property) && Objects.equals(value, that.value) && operator == that.operator && Objects.equals(projectId, that.projectId) && Objects.equals(description, that.description) && Objects.equals(createdOn, that.createdOn) && Objects.equals(updatedOn, that.updatedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, property, value, operator, enabled, projectId, description, createdOn, updatedOn, ruleId);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ConditionsEntity{");
        sb.append("id=").append(id);
        sb.append(", property='").append(property).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append(", operator=").append(operator);
        sb.append(", enabled=").append(enabled);
        sb.append(", projectId=").append(projectId);
        sb.append(", ruleId=").append(ruleId);
        sb.append(", description='").append(description).append('\'');
        sb.append(", createdOn=").append(createdOn);
        sb.append(", updatedOn=").append(updatedOn);
        sb.append('}');
        return sb.toString();
    }
}
