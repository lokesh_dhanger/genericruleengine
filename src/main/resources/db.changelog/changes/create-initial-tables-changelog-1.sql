--liquibase formatted sql
--changeset lokesh.dhanger@daytonatec.com:001

create table projects(
    id bigint not null auto_increment primary key,
    name varchar(255) not null,
    feature varchar(255) not null,
    enabled boolean default false,
    global_type enum('BOOLEAN', 'STRING', 'INTEGER', 'LIST' , 'MAP', 'SET', 'FLOAT') NOT NULL,
    description VARCHAR(1000),
    created_on timestamp default now(),
    updated_on timestamp default now()
);

--rollback DROP TABLE
--rollback projects

create table attributes(
	id bigint not null auto_increment primary key,
	name varchar(255) not null,
	attribute_type varchar(255) not null,
	enabled boolean default false,
	project_id bigint,
	description VARCHAR(1000),
	created_on timestamp default now(),
	updated_on timestamp default now()
);

--rollback DROP TABLE
--rollback attributes

ALTER TABLE attributes
ADD FOREIGN KEY (project_id) REFERENCES projects(id);


create table rules(
	id bigint not null auto_increment primary key,
	rule_name varchar(255) not null,
	enabled boolean default false,
	project_id bigint not null,
	result varchar(255) not null,
	created_on timestamp default now(),
	updated_on timestamp default now()
);

--rollback DROP TABLE
--rollback rules


create table conditions(
	id bigint not null auto_increment primary key,
	property varchar(255) not null,
	value varchar(255) not null,
	enabled boolean default false,
	operator enum('NOT_EQUAL_TO', 'EQUAL_TO','CONTAINS', 'GREATER_THAN', 'LESS_THAN' , 'GREATER_THAN_OR_EQUAL_TO', 'LESS_THAN_OR_EQUAL_TO') NOT NULL,
	project_id bigint not null,
	rule_id bigint not null,
	description VARCHAR(1000),
	created_on timestamp default now(),
	updated_on timestamp default now()
);

--rollback DROP TABLE
--rollback conditions

ALTER TABLE conditions
ADD FOREIGN KEY (project_id) REFERENCES projects(id);


create table rule_templates(
	id bigint not null auto_increment primary key,
	template varchar(5000) not null,
	enabled boolean default false,
	project_id bigint not null,
	created_on timestamp default now(),
	updated_on timestamp default now()
);

--rollback DROP TABLE
--rollback rule_templates

ALTER TABLE rule_templates
ADD FOREIGN KEY (project_id) REFERENCES projects(id);