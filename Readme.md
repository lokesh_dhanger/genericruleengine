#Local Setup

## local database setup
* create a database with name 'ruleengine' on your local
* run the .sql file present in src/main/resources/db/changelog/changes/create-initial-tables-changelog-1.sql
* verify if the tables has been created successfully.

## initial data for local database

* projects table

insert into projects (name, feature, enabled, global_type, description)
values('PGW', 'HOLD_TRANSACTION', true, 'BOOLEAN', 'TEST DESCRIPTION');

insert into projects (name, feature, enabled, global_type, description)
values('MALAICHA', 'LOCATION_PRODUCT_DELIVERY', true, 'LIST', 'TEST DESCRIPTION');

* attributes table

insert into attributes(name, attribute_type, enabled, project_id, description)
values('issuerName', 'String', true, 1, 'test description');

insert into attributes(name, attribute_type, enabled, project_id, description)
values('amount', 'Integer', true, 1, 'test description');

insert into attributes(name, attribute_type, enabled, project_id, description)
values('tenderType', 'String', true, 1, 'test description');


* rules tables

insert into rules(rule_name, enabled, project_id, result)
values('payat-hp, hold transaction, if amount>199900 OR tender type = credit card OR debit card', true, 1, 'true');

insert into rules(rule_name, enabled, project_id, result)
values('bas-hp, hold transaction, if tender type = CHEQUE ', true, 1, 'true');

insert into rules(rule_name, enabled, project_id, result)
values('payat-hp-bank, hold transaction, if amount>550000 AND tender type = credit card OR debit card  ', true, 1, 'true');

* conditions tables

insert into conditions(property, operator, value, enabled, project_id, rule_id, description)
values('issuerName', 'EQUAL_TO', 'pay-at', true, 1, 1, 'test description');

insert into conditions(property, operator, value, enabled, project_id, rule_id, description)
values('tenderType', 'EQUAL_TO', 'CREDIT_CARD', true, 1, 1, 'test description');

insert into conditions(property, operator, value, enabled, project_id, rule_id, description)
values('tenderType', 'EQUAL_TO', 'DEBIT_CARD', true, 1, 1, 'test description');

insert into conditions(property, operator, value, enabled, project_id, rule_id, description)
values('amount', 'GREATER_THAN', '199900', true, 1, 1, 'test description');



## local redis setup

## APIs
### decision tables
* This API includes uploading a valid decision table (excel sheet) file with XLS extension.
* validating the decision table file
* test rule
* create a new stateless session having all new rules
### dynamic rule templates
* Using dynamic rule templates to to add/remove/update existing rules dynamically in session
* 